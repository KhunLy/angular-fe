import { HttpClient, HttpParams } from '@angular/common/http';
import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CountryModel } from '../../models/country.model';

@Component({
  selector: 'app-demo3',
  templateUrl: './demo3.component.html',
  styleUrls: ['./demo3.component.scss']
})
export class Demo3Component implements OnInit, AfterViewInit {


  model: CountryModel[];

  selectedCode: string;

  name: string;

  model2: any

  @ViewChild('select')
  select: ElementRef

  constructor(
    private http: HttpClient,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.model = this.route.snapshot.data.countryModel;
  }

  ngAfterViewInit() {
    M.FormSelect.init(this.select.nativeElement);
  }

  search() {
    let params = new HttpParams();
    params = params.append('name', this.name);
    params = params.append('country_id', this.selectedCode);
    this.http.get<any>('https://api.genderize.io', {params}).subscribe(data => this.model2 = data)
  }
}
