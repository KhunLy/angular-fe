import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoRoutingModule } from './demo-routing.module';
import { DemoComponent } from './demo.component';
import { Demo1Component } from './components/demo1/demo1.component';
import { Demo2Component } from './components/demo2/demo2.component';
import { HighlightDirective } from './directives/highlight.directive';
import { ToFarenheitPipe } from './pipes/to-farenheit.pipe';
import { HttpClientModule } from '@angular/common/http';
import { ToImageOWPipe } from './pipes/to-image-ow.pipe';
import { Demo3Component } from './components/demo3/demo3.component';
import { FormsModule } from '@angular/forms';
import { CountryResolver } from './resolvers/country.resolver';


@NgModule({
  declarations: [DemoComponent, Demo1Component, Demo2Component, HighlightDirective, ToFarenheitPipe, ToImageOWPipe, Demo3Component],
  imports: [
    CommonModule,
    DemoRoutingModule,
    // permet d'utiliser ngModel
    FormsModule,
    // le module permettant d'utiliser le service HttpClient
    HttpClientModule
  ],
  providers: [ CountryResolver ]
})
export class DemoModule { }
